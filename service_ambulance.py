# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import datetime, timedelta, date
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Get
from trytond.transaction import Transaction
from trytond.pool import Pool
import os


class ServiceAmbulance(Workflow, ModelSQL, ModelView):
    'Form'
    __name__ = 'ambulance.service_ambulance'
    movil = fields.Many2One('maintenance.equipment', 'Movil')
    consecutive = fields.Char('Consecutive', readonly=True)
    city = fields.Char('City')
    date = fields.Date('Date', required=True)
    time = fields.Time('Time', required=True)
    type_emergency = fields.Selection([
            ('transit_accident', 'Transit Accident'),
            ('private_transfer', 'Private Transfer'),
            ('transfer_between_clinics', 'Transfer Between Clinics'),
            ('agreement', 'Agreement'),
            ('social', 'Social'),
            ('work_accident', 'Work Accident'),
            ('student_accident', 'Student Accident'),
        ], 'Type Emergency', select=True)
    insurance = fields.Char('Insurance', required=True)
    insurance_policy = fields.Numeric('Policy', digits=(12,0), required=True)
    patient = fields.Many2One('party.party', 'Patient')
    eps = fields.Many2One('party.party', 'EPS/ARS')
    origin_accident = fields.Char('Origin')
    zone = fields.Selection([
        ('urban', 'Urban'),
        ('rural', 'Rural'),
        ], 'Zone', select=True)
    destination = fields.Many2One('party.party', 'Destination')
    start_time = fields.Time('Start Route', required=True)
    end_time = fields.Time('End Route', required=True)
    vital_signs_ta = fields.Char('TA:145/71', required=True)
    vital_signs_fc = fields.Numeric('FC:68x1', digits=(3,0), required=True)
    vital_signs_fr = fields.Numeric('FR:24x1', digits=(3,0), required=True)
    vital_signs_temp = fields.Numeric('TEMP:°C', digits=(2,0), required=True)
    vital_signs_saturation = fields.Char('SATURATION %', required=True)
    head = fields.Char('Head')
    pupils = fields.Char('Pupils')
    neck = fields.Char('Neck')
    rib_cage = fields.Char('Rib Cage')
    ms = fields.Char('MS')
    abdomen = fields.Char('Abdomen')
    pelvis = fields.Char('Pelvis')
    back = fields.Char('Back')
    mi = fields.Char('MI')
    conscious = fields.Selection([
            ('yes', 'Yes'),
            ('no', 'No'),
        ], 'Conscious', select=True)
    oriented = fields.Selection([
            ('yes', 'Yes'),
            ('no', 'No'),
        ], 'Oriented', select=True)
    alert = fields.Selection([
            ('yes', 'Yes'),
            ('no', 'No'),
        ], 'Alert', select=True)
    sleepy = fields.Selection([
            ('yes', 'Yes'),
            ('no', 'No'),
        ], 'Sleepy', select=True)
    aggressive = fields.Selection([
            ('yes', 'Yes'),
            ('no', 'No'),
        ], 'Aggressive', select=True)
    answer_back = fields.Selection([
            ('yes', 'Yes'),
            ('no', 'No'),
        ], 'Answer Back', select=True)
    num_siras = fields.Char('Siras')
    notes = fields.Text('Notes')
    supplies = fields.Text('Supplies Used')
    responsible_of_service_paramedic = fields.Many2One('company.employee', 'Paramedic')
    responsible_of_service_driver = fields.Many2One('company.employee', 'Driver')

class ServiceAmbulanceBitacoraReport(Report):
    __name__ = 'ambulance.bitacora.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ServiceAmbulanceBitacoraReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        date_ = date.today()
        report_context['today'] = date_
        return report_context

class ServiceAmbulanceFurtranReport(Report):
    __name__ = 'ambulance.furtran.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ServiceAmbulanceFurtranReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        Config = Pool().get('ambulance.configuration')
        config = Config.search([])
        report_context['company'] = user.company
        report_context['user'] = user
        report_context['enablement_code'] = config[0].enablement_code
        print(config[0].enablement_code)
        date_ = date.today()
        report_context['today'] = date_
        return report_context
